package endpoint

import (
	"context"
	service "ms-demo-docker-covid19/pkg/service"

	endpoint "github.com/go-kit/kit/endpoint"
)

// GetCovidStatisticsByCountryCodeRequest collects the request parameters for the GetCovidStatisticsByCountryCode method.
type GetCovidStatisticsByCountryCodeRequest struct{}

// GetCovidStatisticsByCountryCodeResponse collects the response parameters for the GetCovidStatisticsByCountryCode method.
type GetCovidStatisticsByCountryCodeResponse struct {
	I0 interface{} `json:"data"`
	E1 error       `json:"error"`
}

// MakeGetCovidStatisticsByCountryCodeEndpoint returns an endpoint that invokes GetCovidStatisticsByCountryCode on the service.
func MakeGetCovidStatisticsByCountryCodeEndpoint(s service.MsDemoDockerCoronavirusService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		i0, e1 := s.GetCovidStatisticsByCountryCode(ctx)
		return GetCovidStatisticsByCountryCodeResponse{
			E1: e1,
			I0: i0,
		}, nil
	}
}

// Failed implements Failer.
func (r GetCovidStatisticsByCountryCodeResponse) Failed() error {
	return r.E1
}

// Failure is an interface that should be implemented by response types.
// Response encoders can check if responses are Failer, and if so they've
// failed, and if so encode them using a separate write path based on the error.
type Failure interface {
	Failed() error
}

// GetCovidStatisticsByCountryCode implements Service. Primarily useful in a client.
func (e Endpoints) GetCovidStatisticsByCountryCode(ctx context.Context) (i0 interface{}, e1 error) {
	request := GetCovidStatisticsByCountryCodeRequest{}
	response, err := e.GetCovidStatisticsByCountryCodeEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetCovidStatisticsByCountryCodeResponse).I0, response.(GetCovidStatisticsByCountryCodeResponse).E1
}

// GetCovidStatisticsRequest collects the request parameters for the GetCovidStatistics method.
type GetCovidStatisticsRequest struct{}

// GetCovidStatisticsResponse collects the response parameters for the GetCovidStatistics method.
type GetCovidStatisticsResponse struct {
	I0 interface{} `json:"i0"`
	E1 error       `json:"e1"`
}

// MakeGetCovidStatisticsEndpoint returns an endpoint that invokes GetCovidStatistics on the service.
func MakeGetCovidStatisticsEndpoint(s service.MsDemoDockerCoronavirusService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		i0, e1 := s.GetCovidStatistics(ctx)
		return GetCovidStatisticsResponse{
			E1: e1,
			I0: i0,
		}, nil
	}
}

// Failed implements Failer.
func (r GetCovidStatisticsResponse) Failed() error {
	return r.E1
}

// GetCovidStatistics implements Service. Primarily useful in a client.
func (e Endpoints) GetCovidStatistics(ctx context.Context) (i0 interface{}, e1 error) {
	request := GetCovidStatisticsRequest{}
	response, err := e.GetCovidStatisticsEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetCovidStatisticsResponse).I0, response.(GetCovidStatisticsResponse).E1
}
