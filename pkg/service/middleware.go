package service

import (
	"context"

	log "github.com/go-kit/kit/log"
)

// Middleware describes a service middleware.
type Middleware func(MsDemoDockerCoronavirusService) MsDemoDockerCoronavirusService

type loggingMiddleware struct {
	logger log.Logger
	next   MsDemoDockerCoronavirusService
}

// LoggingMiddleware takes a logger as a dependency
// and returns a MsDemoDockerCoronavirusService Middleware.
func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next MsDemoDockerCoronavirusService) MsDemoDockerCoronavirusService {
		return &loggingMiddleware{logger, next}
	}

}

func (l loggingMiddleware) GetCovidStatisticsByCountryCode(ctx context.Context) (i0 interface{}, e1 error) {
	defer func() {
		l.logger.Log("method", "GetCovidStatisticsByCountryCode", "i0", i0, "e1", e1)
	}()
	return l.next.GetCovidStatisticsByCountryCode(ctx)
}

func (l loggingMiddleware) GetCovidStatistics(ctx context.Context) (i0 interface{}, e1 error) {
	defer func() {
		l.logger.Log("method", "GetCovidStatistics", "i0", i0, "e1", e1)
	}()
	return l.next.GetCovidStatistics(ctx)
}
