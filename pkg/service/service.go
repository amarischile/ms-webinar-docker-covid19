package service

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

// MsDemoDockerCoronavirusService describes the service.
type MsDemoDockerCoronavirusService interface {
	// Add your methods here
	GetCovidStatisticsByCountryCode(ctx context.Context) (interface{}, error)
	GetCovidStatistics(ctx context.Context) (interface{}, error)
}

type basicMsDemoDockerCoronavirusService struct{}

type ResponseCovidStatistics struct {
	Country        string  `json:"country"`
	CountryCode    string  `json:"iso"`
	TotalConfirmed float64 `json:"confirmed"`
	TotalRecovered   float64 `json:"recovered"`
	TotalDeaths    float64 `json:"deaths"`
}

func (b *basicMsDemoDockerCoronavirusService) GetCovidStatisticsByCountryCode(ctx context.Context) (i0 interface{}, e1 error) {
	countryCode := strings.ToUpper(os.Getenv("COUNTRY_CODE"))
	var response interface{} = nil
	resp, err := http.Get(os.Getenv("COVID_URL"))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	dataBytes, _ := ioutil.ReadAll(resp.Body)
	var data interface{}

	json.Unmarshal(dataBytes, &data)
	dataMap := data.(map[string]interface{})
	countries := dataMap["Countries"].([]interface{})

	for _, value := range countries {
		country := value.(map[string]interface{})
		if country["CountryCode"].(string) == countryCode {
			response = mapToResponse(country)
		}
	}

	return response, nil
}

// NewBasicMsDemoDockerCoronavirusService returns a naive, stateless implementation of MsDemoDockerCoronavirusService.
func NewBasicMsDemoDockerCoronavirusService() MsDemoDockerCoronavirusService {
	return &basicMsDemoDockerCoronavirusService{}
}

// New returns a MsDemoDockerCoronavirusService with all of the expected middleware wired in.
func New(middleware []Middleware) MsDemoDockerCoronavirusService {
	var svc MsDemoDockerCoronavirusService = NewBasicMsDemoDockerCoronavirusService()
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}

func (b *basicMsDemoDockerCoronavirusService) GetCovidStatistics(ctx context.Context) (i0 interface{}, e1 error) {
	// TODO implement the business logic of GetCovidStatistics
	return i0, e1
}

func mapToResponse(country map[string]interface{}) ResponseCovidStatistics {
	var c = ResponseCovidStatistics{}
	c.Country = country["Country"].(string)
	c.CountryCode = country["CountryCode"].(string)
	c.TotalConfirmed = country["TotalConfirmed"].(float64)
	c.TotalRecovered = country["TotalRecovered"].(float64)
	c.TotalDeaths = country["TotalDeaths"].(float64)
	return c
}
