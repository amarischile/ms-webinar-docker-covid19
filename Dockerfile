FROM golang:latest as builder

ENV SERVICE_NAME=ms-demo-docker-covid19
ENV APP /src/${SERVICE_NAME}/
ENV WORKDIR ${GOPATH}${APP}

WORKDIR $WORKDIR
ADD . $WORKDIR

RUN go get -d -v ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o $SERVICE_NAME cmd/main.go

FROM alpine

ENV SERVICE_NAME=ms-demo-docker-covid19
ENV APP /src/${SERVICE_NAME}/
ENV GOPATH /go
ENV WORKDIR ${GOPATH}${APP}

COPY --from=builder ${WORKDIR}${SERVICE_NAME} $WORKDIR

WORKDIR $WORKDIR
CMD ./${SERVICE_NAME}